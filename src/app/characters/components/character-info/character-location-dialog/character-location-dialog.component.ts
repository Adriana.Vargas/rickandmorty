import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-character-location-dialog',
  templateUrl: './character-location-dialog.component.html',
  styleUrls: ['./character-location-dialog.component.scss']
})
export class CharacterLocationDialogComponent implements OnInit {
  @Input() locationInfo: any;

  constructor(
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
  }

  public closeModal() {
    this.modalService.dismissAll();
  }

}
