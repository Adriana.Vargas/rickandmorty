import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterLocationDialogComponent } from './character-location-dialog.component';

describe('CharacterLocationDialogComponent', () => {
  let component: CharacterLocationDialogComponent;
  let fixture: ComponentFixture<CharacterLocationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterLocationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterLocationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
