import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { apiService } from '../../../shared/services/api.service';
import { Character } from '../../models/character.model';
import { CharacterLocationDialogComponent } from './character-location-dialog/character-location-dialog.component';

@Component({
  selector: 'app-character-info',
  templateUrl: './character-info.component.html',
  styleUrls: ['./character-info.component.scss']
})
export class CharacterInfoComponent implements OnInit {
  @Input() character: Character;
  public characterData: {
    image: string, 
    name: string, 
    status: string, 
    species: string, 
    episode: string, 
    lastLocation: string
  };
  
  constructor(
    private apiService: apiService,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    if (this.character) {
    this.apiService.getEpisode(this.character.episode[0]).subscribe((data: any) => {
      this.characterParse(data.name);
    });   
    }; 
  }

  public characterParse(episodeNm) {
    this.characterData = {
      image: this.character.image || '',
      name: this.character.name || '',
      status: this.character.status || '',
      species: this.character.species || '',
      episode: episodeNm || '',
      lastLocation: this.character.location.name || ''
    }; 
  }

  public openLocationModal(locationInfo) {
    const modalRef = this.modalService.open(CharacterLocationDialogComponent, {
      centered: true,
      size: "md",
    });
    modalRef.componentInstance.locationInfo = locationInfo;
  }

  public getLocationInfo(url) {
    this.apiService.getCharacterLocationInfo(url).subscribe((data: any) => {
      this.openLocationModal(data)
    });  
  }

}
