import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { CharacterListData } from '../../models/character.model';

@Component({
  selector: 'app-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.scss']
})
export class CharactersListComponent implements OnInit {
  @Input() charactersList: CharacterListData;
  @Output() onGetCharactersPage: EventEmitter<any> = new EventEmitter();

  public page: number = 1;
  public pageSize: number = 10;
  public collectionSize: number;

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.charactersList) {
      this.collectionSize = this.charactersList.info.count;
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    }
  }

  public pagination(page) {
    this.onGetCharactersPage.emit(page);
  }

}
