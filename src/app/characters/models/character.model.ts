export interface CharacterListData {
    info: Info;
    results: Character[];

}

export interface Info {
    count: number;
    pages: number;
    next: string;
    prev: string;

}

export interface Character {
    id: number;
    name: string;
    status: string;
    species: string;
    type: string;
    gender: string;
    location: Location;
    origin: Origin;
    image: string;
    episode: any[];
    url: string;
    created: string;
}

export interface Location {
    name: string;
    url: string;
}

export interface Origin {
    name: string;
    url: string;
}