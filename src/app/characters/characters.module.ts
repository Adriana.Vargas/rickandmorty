import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharactersListComponent } from './components/characters-list/characters-list.component';
import { CharacterInfoComponent } from './components/character-info/character-info.component';
import { CharactersListPageComponent } from './containers/characters-list-page/characters-list-page.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/services/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CharacterLocationDialogComponent } from './components/character-info/character-location-dialog/character-location-dialog.component';

const routes: Routes = [
  { path: '', component: CharactersListPageComponent},
  
]

@NgModule({
  declarations: [
    CharactersListComponent, 
    CharacterInfoComponent, 
    CharactersListPageComponent, 
    CharacterLocationDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    NgbModule 
  ],
  entryComponents: [
    CharacterLocationDialogComponent
  ]
})
export class CharactersModule { }
