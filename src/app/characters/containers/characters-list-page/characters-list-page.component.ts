import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { apiService } from '../../../shared/services/api.service';
import { catchError, map, tap } from 'rxjs/operators';
import { CharacterListData } from '../../models/character.model';

@Component({
  selector: 'app-characters-list-page',
  templateUrl: './characters-list-page.component.html',
  styleUrls: ['./characters-list-page.component.scss']
})
export class CharactersListPageComponent implements OnInit {
  public characters$: CharacterListData;
  public page: number = 1;

  constructor(
    private apiService: apiService
  ) { }

  ngOnInit() {
    this.getCharacters(this.page);  
  }

  public getCharacters(page) {
    this.apiService.getListCharacters(page).subscribe((data) => {
      this.characters$ = data;
    });
  }

}
