import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class apiService {

  public apiUrl: string = 'https://rickandmortyapi.com/api'

  constructor(
    private http: HttpClient,
  ) { }

  public getListCharacters(page: number): any {
    return this.http.get(`${this.apiUrl}/character?page=${page}`);
  }

  public getEpisode(url) {
    return this.http.get(url);
  }

  public getCharacterLocationInfo(url) {
    return this.http.get(url);
  }

  public getAllEpisodes(page) {
    return this.http.get(`${this.apiUrl}/episode?page=${page}`);
  }

}
