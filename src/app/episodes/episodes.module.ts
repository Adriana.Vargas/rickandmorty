import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EpisodesListComponent } from './components/episodes-list/episodes-list.component';
import { EpisodesListPageComponent } from './containers/episodes-list-page/episodes-list-page.component';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/services/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EpisodeInfoComponent } from './components/episode-info/episode-info.component';

const routes: Routes = [
  { path: '', component: EpisodesListPageComponent }
]

@NgModule({
  declarations: [
    EpisodesListComponent, 
    EpisodesListPageComponent, EpisodeInfoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    NgbModule
  ]
})
export class EpisodesModule { }
