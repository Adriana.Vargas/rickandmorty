export interface EpisodeListData {
    info: Info;
    results: Episode[];

}

export interface Info {
    count: number;
    pages: number;
    next: string;
    prev: string;
}

export interface Episode {
    air_date: string;
    characters: string[];
    created: string;
    episode: string;
    id: number;
    name: string;
    url: string;
}