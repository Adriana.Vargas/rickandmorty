import { Component, OnInit } from '@angular/core';
import { apiService } from '../../../shared/services/api.service';
import { EpisodeListData } from '../../models/episode.model';

@Component({
  selector: 'app-episodes-list-page',
  templateUrl: './episodes-list-page.component.html',
  styleUrls: ['./episodes-list-page.component.scss']
})
export class EpisodesListPageComponent implements OnInit {
  public episodesList$: any;
  public page: number = 1;

  constructor(
    private apiService: apiService
  ) { }

  ngOnInit() {
    this.onGetEpisodesPage(this.page);
  }

  public onGetEpisodesPage(page) {
     this.apiService.getAllEpisodes(page).subscribe((data) => {
      this.episodesList$ = data;
    });
  }

}
