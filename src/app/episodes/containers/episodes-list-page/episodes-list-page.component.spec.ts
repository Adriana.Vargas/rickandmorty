import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpisodesListPageComponent } from './episodes-list-page.component';

describe('EpisodesListPageComponent', () => {
  let component: EpisodesListPageComponent;
  let fixture: ComponentFixture<EpisodesListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpisodesListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpisodesListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
