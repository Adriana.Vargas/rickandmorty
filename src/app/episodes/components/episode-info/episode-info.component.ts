import { Component, Input, OnInit } from '@angular/core';
import { Episode } from '../../models/episode.model';
import { apiService } from '../../../shared/services/api.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-episode-info',
  templateUrl: './episode-info.component.html',
  styleUrls: ['./episode-info.component.scss']
})
export class EpisodeInfoComponent implements OnInit {
  @Input() episode: Episode;

  constructor(
  ) { }

  ngOnInit() {}

}

