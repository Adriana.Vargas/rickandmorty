import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EpisodeListData } from '../../models/episode.model';

@Component({
  selector: 'app-episodes-list',
  templateUrl: './episodes-list.component.html',
  styleUrls: ['./episodes-list.component.scss']
})
export class EpisodesListComponent implements OnInit {
  @Input() episodesList: EpisodeListData;
  @Output() onGetEpisodesPage: EventEmitter<any> = new EventEmitter();
  public page: number = 1;
  public pageSize: number = 10;
  public collectionSize: number = 30;

  constructor() { }

  ngOnInit() {
    if (this.episodesList) {
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    }
  }

  public pagination(page) {
    this.onGetEpisodesPage.emit(page);
  }

}
